<!--
  #%L
  Play SQL Sign-Off
  %%
  Copyright (C) 2016 Play SQL S.A.S.U.
  %%
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
       http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  #L%
  -->
<atlassian-plugin key="${project.groupId}.${project.artifactId}" name="${project.name}" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}" />
        <!--<param name="configure.url">/admin/plugins/${project.groupId}.${project.artifactId}/global-admin.action</param>-->

        <param name="plugin-icon">img/sign-off-logo-16x.png</param>
        <param name="plugin-logo">img/sign-off-logo-72x.png</param>

        <param name="plugin-banner">img/Sign Off Banner.png</param>
        <param name="vendor-icon">img/vendor-icon.png</param>
        <param name="vendor-logo">img/vendor-logo.png</param>

        <param name="atlassian-data-center-compatible">true</param>
    </plugin-info>

    <resource type="i18n" name="Internationalization" location="com/playsql/signoff-i18n" />
    <resource key="img" name="img/" type="download" location="img" />

    <web-resource name="JS / CSS for the button in the View page" key="signoff-view">
        <transformation extension="js">
            <transformer key="jsI18n"/>
        </transformation>
        <transformation extension="soy">
            <transformer key="soyTransformer">
                <functions>com.atlassian.confluence.plugins.soy:soy-core-functions</functions>
            </transformer>
        </transformation>
        <resource type="download" name="signoff.js" location="js/signoff.js" />
        <resource type="download" name="signoff.css" location="js/signoff.css" />
        <resource type="download" name="signoff-soy.js" location="js/signoff.soy" />
        <dependency>com.atlassian.auiplugin:aui-select2</dependency>
        <dependency>com.atlassian.auiplugin:aui-flag</dependency>
        <dependency>com.atlassian.auiplugin:aui-experimental-tooltips</dependency>
        <dependency>com.atlassian.auiplugin:aui-experimental-table-sortable</dependency>

        <context>viewcontent</context>
        <context>cq.feature.viewquestion</context>
    </web-resource>

    <web-item key="signoff" name="Sign Off" section="system.content.button" weight="70">
        <label key="com.playsql.signoff.toolbar.sign"/>
        <tooltip key="com.playsql.signoff.toolbar.sign.tooltip"/>
        <link linkId="signoff-button"></link>
        <condition class="com.atlassian.confluence.plugin.descriptor.web.conditions.ViewingContentCondition" />
		<condition class="com.atlassian.confluence.plugin.descriptor.web.conditions.PagePermissionCondition">
			<param name="permission">edit</param>
		</condition>
        <styleClass>signoff-menu</styleClass>
        <param name="iconClass">aui-icon aui-icon-small aui-iconfont-approve</param>
    </web-item>

    <web-panel key="signoff-metadata-blogs" location="atl.page.metadata.banner">
        <context-provider class="com.playsql.signoff.web.MetadataContextProvider" />
       <resource name="view" type="velocity" location="metadata-head.vm"/>
   </web-panel>

    <component key="signoffService" class="com.playsql.signoff.manager.SignoffService"  name="Service to sign off documents"/>

    <component-import key="application-properties" interface="com.atlassian.sal.api.ApplicationProperties"  name="Import - Application Properties"/>
    <component-import key="context-path-holder" name="Context Path reference" interface="com.atlassian.confluence.core.ContextPathHolder"/>
    <component-import key="settings-manager" name="Settings manager" interface="com.atlassian.confluence.setup.settings.SettingsManager"/>
    <component-import key="ao" name="Active Objects service" interface="com.atlassian.activeobjects.external.ActiveObjects"/>
    <component-import key="transactions" name="Transactions" interface="com.atlassian.sal.api.transaction.TransactionTemplate"/>
    <component-import key="sidebar-link-service" interface="com.atlassian.confluence.plugins.ia.service.SidebarLinkService" />
    <component-import key="plugin-settings-factory" interface="com.atlassian.sal.api.pluginsettings.PluginSettingsFactory" name="Plugins settings"/>
    <component-import key="ceo-manager" interface="com.atlassian.confluence.core.ContentEntityManager" name="CEO Manager"/>
    <component-import key="page-manager" interface="com.atlassian.confluence.pages.PageManager" name="Abstract Page Manager"/>
    <component-import key="breadcrump-generator" interface="com.atlassian.confluence.util.breadcrumbs.BreadcrumbGenerator" name="Breadcrumb Generator"/>
    <component-import key="applinks-service" interface="com.atlassian.applinks.api.ApplicationLinkService" name="Applinks"/>
    <component-import key="entitylinks-service" interface="com.atlassian.applinks.api.EntityLinkService" name="Applinks for entities"/>
    <component-import key="locale-manager" interface="com.atlassian.confluence.languages.LocaleManager" name="Locale Manager"/>
    <component-import key="i81n-factory" interface="com.atlassian.confluence.util.i18n.I18NBeanFactory" name="Internationalization"/>
    <component-import key="feature-discovery-service" interface="com.atlassian.confluence.plugins.featurediscovery.service.FeatureDiscoveryService"/>

    <rest key="rest" path="/signoff" version="1" name="REST resources">
        <description>REST API for the Signoff tool.</description>
        <package>com.playsql.signoff.rest</package>
    </rest>

</atlassian-plugin>
