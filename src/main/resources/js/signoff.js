/*
 * #%L
 * Play SQL Sign-Off
 * %%
 * Copyright (C) 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
AJS.$(function($){require(['aui/flag'], function(flag) {

    var $button = $("#signoff-button");
    function abbreviate(message, length) {
        if (message && message.length > length)
            message = message.substr(0, length - 50) + "...";
        return message;
    }
    function setButton(text, message, css) {
        if (text) {
            var textNode = $button.find("span").contents().filter(function(){
                return this.nodeType==3 && $.trim(this.textContent).length > 0;
            });
            if (textNode) {
                textNode[0].textContent = " " + text;
            }
        }
        if (message) $button.attr("title", message).tooltip({delayIn: 0});
        if (css) $button.addClass(css);
    }
    function getCustomMeta(key) {
        return $("meta[name='" + key + "']").attr("content");
    }

    var contentId = AJS.Meta.get("page-id");
    var hash = getCustomMeta("signoff-addon-page-hash");
    if (hash) {
        var status = getCustomMeta("signoff-addon-request-status");
        var requestor = getCustomMeta("signoff-addon-requestor");
        var signators = getCustomMeta("signoff-addon-signators");
        var countOlderVersionsSigned = getCustomMeta("signoff-addon-count-versions-signed");
        var olderSignators = getCustomMeta("signoff-addon-older-signators");
        var percentageCompleted = parseInt(getCustomMeta("signoff-addon-request-percentage"));

        var message = "Click to sign off this page.";
        var css = "";
        var text = "Sign off";
        if (status == "SEEN") {
            message = requestor + " has requested that you sign this document.";
            css = "signoff-requested";
            text = "Signature Requested";
        } else if (status == "SIGNED") {
            message = "You have signed this document.";
            css = "signed-same-version";
            text = "Signed by you";
        } else if (status == "OUTDATED") {
            message = "You have signed an older version of this document";
            css = "signed-other-version";
            text = "Signed by you*";
        } else if (!isNaN(percentageCompleted) && percentageCompleted < 100) {
            // That's the situation where a person wants others to sign and monitors how many have
            message = "Signed by " + percentageCompleted + "% of users who were requested to sign.";
            css = "signed-other-version";
            text = percentageCompleted + "% Signed";
        } else if (signators) {
            message = "Signed by " + signators + ". Click to see the details.";
            css = "signed-same-version";
            text = "Signed off";
        } else if (countOlderVersionsSigned == 1) {
            message = "A previous version was signed by " + olderSignators + ". Click to see the details.";
            css = "signed-other-version";
            text = "Signed off*";
        } else if (countOlderVersionsSigned > 1) {
            message = countOlderVersionsSigned + " previous versions were signed, but there have been modifications since then. Click to see the details.";
            css = "signed-other-version";
            text = "Signed off*";
        }
        setButton(text, message, css);
    } else {
        setButton(null, "Couldn't retrieve the ID of the current page.");
    }

    var inlineDialog = undefined;

    $button.click(function(e){
        e.preventDefault();
        if (!contentId) {
            flag({
                type: 'error',
                title: "The popup can't be displayed",
                persistent: true,
                close: "manual",
                body: "Couldn't find the ID of the document"
            });
            return false;
        }
        closeDialog(true);

        $.ajax({
            url: AJS.contextPath() + "/rest/signoff/1/signoff/" + contentId + "?" + $.param({
                currentHash: hash
            })
        }).success(function(data) {
            inlineDialog = AJS.InlineDialog($button, "signoff-detail", function($content, trigger, callback) {
                var currentSignatures = data.signatures[-1];
                var isCurrentPageSigned = currentSignatures && currentSignatures.length > 0;
                $content.html(PlaySQL.Signoff.detail({
                    data: data.signatures,
                    versionsInReverseOrder: Object.keys(data.signatures).reverse(),
                    requests: data.requests,
                    signaturesOutsideRequests: data.signaturesOutsideRequests,
                    historyVersion: getCustomMeta("signoff-addon-viewing-history-version"),
                    status: status,
                    isCurrentPageSigned: isCurrentPageSigned
                }));
                $(".tooltipable").tooltip({delayIn: 0});
                callback();
            }, {
                showDelay : 0,
                //hideDelay : 600000,
                width: 600,
                persistent: true,
                cacheContent: false,
                useLiveEvents: false,
                noBind: true,
                hideCallback: function() {
                    inlineDialog = undefined;
                }
            });
            inlineDialog.show();
            $("#inline-dialog-signoff-detail").on("click", "#signoff-now-button", function(e){
                e.preventDefault();
                if (!contentId) {
                    flag({
                        type: 'error',
                        title: "The document couldn't be signed off",
                        persistent: true,
                        close: "manual",
                        body: "Couldn't find the ID of the document"
                    });
                    return false;
                }
                $.ajax({
                    url: AJS.contextPath() + "/rest/signoff/1/signoff/" + contentId,
                    type: "PUT"
                }).success(function(data) {
                    flag({
                        type: 'success',
                        title: "Success",
                        persistent: false,
                        close: "auto",
                        body: AJS.escapeHtml("Signed!")
                    });
                    setButton("Signed by you", "signed-current-version");
                }).fail(function(jqXHR) {
                    var message = abbreviate(jqXHR.responseText, 250);
                    flag({
                        type: 'error',
                        title: "The document couldn't be signed off",
                        persistent: true,
                        close: "manual",
                        body: AJS.escapeHtml(jqXHR.status + "-" + jqXHR.statusText + ": " + message)
                    });
                });
                closeDialog();
                return false;
            }).on("click", "#signoff-request-button", function(e){
                e.preventDefault();
                $("#inline-dialog-signoff-detail .contents").html(PlaySQL.Signoff.request({
                    //data: data
                }));
                $(".tooltipable").tooltip({delayIn: 0});
                $("#signoff-request-groups").auiSelect2({
                    multiple: true,
                    placeholder: "Add users or groups",
                    formatResult: function (item) {
                        return PlaySQL.Signoff.formatUser({item:item, "short": false });
                    },
                    formatSelection: function (item) {
                        return PlaySQL.Signoff.formatUser({item:item, "short": true});
                    },
                    escapeMarkup: function (m) {
                        if (window.a) eval("debugger");
                        // Display text as html
                        return m;
                    },
                    formatNoMatches: function (term) {
                        return "No matches"
                    },
                    minimumInputLength: 2,
                    query: function (query) {
                        var q = query.term;
                        $.ajax({
                            url: AJS.contextPath() + "/rest/prototype/1/search/user-or-group.json?" + $.param({
                                query: q,
                                "max-results": 10
                            }),
                            type: 'GET',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json"
                        }).success(function (data) {
                            searchResults = $.map(data.result, function (result) {
                                if (result.type == "user") {
                                    return {
                                        id: "user-" + result.userKey,
                                        text: "User " + result.title,
                                        shortText: result.title,
                                        type: "user",
                                        userKey: result.userKey,
                                        username: result.username,
                                        img: result.thumbnailLink.href
                                    }
                                } else if (result.type == "group") {
                                    return {
                                        id: "group-" + result.name,
                                        text: "Group " + result.name,
                                        shortText: result.name,
                                        img: AJS.contextPath() + "/download/resources/com.atlassian.confluence.plugins.share-page:mail-page-resources/images/group.png"
                                    }
                                }
                            });
                            query.callback({results: searchResults});
                        }).error(function(){
                            query.callback({results: [{id: {}, text: "Couldn't fetch the users/groups"}]});
                        });
                    },
                    initSelection: function ($element, callback) {
                        callback([]);
                    }
                });
            }).on("click", "#signoff-request-submit", function(e){
                var recipients = $("#signoff-request-groups").auiSelect2("val"); // Returns all IDs as an array
                var message = $("#signoff-message").val();
                $.ajax({
                    url: AJS.contextPath() + "/rest/signoff/1/request/" + contentId,
                    type: "PUT",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({
                        message: message,
                        recipients: recipients,
                        sendMail: true
                    }),
                    dataType: "json"
                }).success(function(data){
                    flag({
                        type: 'success',
                        title: "Sent!",
                        persistent: false,
                        close: "auto",
                        body: "The link was sent to the recipient."
                    });
                    closeDialog();
                }).fail(function(jqXHR){
                    var message = abbreviate(jqXHR.responseText, 250);
                    flag({
                        type: 'error',
                        title: "Can't send the sign-off request",
                        persistent: true,
                        close: "manual",
                        body: AJS.escapeHtml(jqXHR.status + "-" + jqXHR.statusText + ": " + message)
                    });
                });
            }).on("click", "#signoff-ignore-button", function(e){
                $.ajax({
                    url: AJS.contextPath() + "/rest/signoff/1/request/" + contentId,
                    type: "DELETE"
                }).success(function(data){
                    flag({
                        type: 'success',
                        title: "Ignored",
                        persistent: false,
                        close: "auto",
                        body: "The sign-off was ignored."
                    });
                    closeDialog();
                }).fail(function(jqXHR){
                    var message = abbreviate(jqXHR.responseText, 250);
                    flag({
                        type: 'error',
                        title: "Error while ignoring a request",
                        persistent: true,
                        close: "manual",
                        body: AJS.escapeHtml(jqXHR.status + "-" + jqXHR.statusText + ": " + message)
                    });
                    closeDialog();
                });
            }).on("click", "#signoff-close-button", function(e){
                e.preventDefault();
                closeDialog();
                return false;
            }).on("click", ".signoff-toggle-display", function(){
                $(".most-relevant-signatures, .all-signatures").toggleClass("hidden");
            });
        }).fail(function(jqXHR) {
            var message = abbreviate(jqXHR.responseText, 250);
            flag({
                type: 'error',
                title: "Can't display the signatures",
                persistent: true,
                close: "manual",
                body: AJS.escapeHtml(jqXHR.status + "-" + jqXHR.statusText + ": " + message)
            });
        });
        return false;
    });

    function closeDialog(immediate) {
        if (inlineDialog) {
            inlineDialog.hide();
        }
        if (immediate) {
            $("#inline-dialog-signoff-detail").remove();
        } else {
            window.setTimeout(function () {
                $("#inline-dialog-signoff-detail").remove();
            }, 10);
        }
    }

    $("body").on("click", "*", function(e) {
        // Autohide
        var $target = $(e.target);
        if ($("#inline-dialog-signoff-detail").size() > 0
            && $target.closest("#inline-dialog-signoff-detail, .select2-drop").size() == 0
            && $target.closest("body").size() != 0 /* Check the target is still attached to the DOM */) {
            closeDialog(false);
        }
    });

    if (window.location.hash == "#signoff-request") {
        $button.click();
    }

}); // of require
}); // of AJS.toInit