package com.playsql.signoff.beans;

/*
 * #%L
 * Play SQL Sign-Off
 * %%
 * Copyright (C) 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.common.collect.Lists;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.List;

/**
 * A sign-off request that is saved using Bandana with JSON serialization. See {@link SignoffRecord} for
 * my defense against an ActiveObjects implementation.
 *
 * A "request" isn't tightly linked with signatures. It's up to the presentation layer to mix and match who
 * was requested to sign and who denied, viewed or signed.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class SignoffRequest {
    private String authorKey;

    // Recipients, in the form "group-" + groupName or "user-" + userKey
    private List<String> recipients;
    // Recipients who refused, in the form "group-" + groupName or "user-" + userKey
    private List<String> refusals;
    // Recipients who refused, in the form "group-" + groupName or "user-" + userKey
    private List<String> viewed;

    @XmlTransient
    private String authorDisplayName;
    @XmlTransient
    private String authorName;

    public SignoffRequest() {
        // JAXB
    }

    public SignoffRequest(String authorKey, List<String> recipients) {
        this.authorKey = authorKey;
        this.recipients = recipients;
    }

    public String getAuthorKey() {
        return authorKey;
    }

    public List<String> getRecipients() {
        return recipients;
    }

    public String getAuthorDisplayName() {
        return authorDisplayName;
    }

    public void setAuthorDisplayName(String authorDisplayName) {
        this.authorDisplayName = authorDisplayName;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public List<String> getRefusals() {
        return refusals;
    }

    public List<String> getViewed() {
        return viewed;
    }

    public boolean addViewed(String user) {
        if (viewed == null)
            viewed = Lists.newArrayList();
        if (!viewed.contains(user)) {
            viewed.add(user);
            return true;
        }
        return false;
    }

    public boolean addRefusal(String user) {
        if (refusals == null)
            refusals = Lists.newArrayList();
        if (!refusals.contains(user)) {
            refusals.add(user);
            return true;
        }
        return false;
    }
}
