package com.playsql.signoff.beans;

/*
 * #%L
 * Play SQL Sign-Off
 * %%
 * Copyright (C) 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.user.ConfluenceUser;
import org.apache.commons.codec.digest.DigestUtils;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * The record of a signature that is saved using Bandana with JSON serialization.
 *
 * I'll defend this choice of architecture! Bandana is superior to ActiveObjects because AO is
 * unreliable across DBMS vendors. Bandana is a simple key-value store. Plus Bandana values
 * saved in a space context are exported with the space.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
public class SignoffRecord {
    private String userKey;
    private String date;
    private String hash;
    /** The page version */
    private int version;
    /** The CEO ID of the version of the page, at the time of signature */
    private long pageVersionId;

    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    public SignoffRecord(ConfluenceUser user, Date date, int version, long pageVersionId, String hash) {
        this.userKey = user.getKey().getStringValue();
        this.date = date == null ? null : DATE_FORMAT.format(date);
        this.hash = hash;
        this.version = version;
        this.pageVersionId = pageVersionId;
    }

    public SignoffRecord() {
        // For JAXB/GSON
    }

    public SignoffRecord(SignoffRecord record) {
        this.userKey = record.userKey;
        this.date = record.date;
        this.hash = record.hash;
        this.version = record.version;
        this.pageVersionId = record.pageVersionId;
    }

    public String getUserKey() {
        return userKey;
    }

    public String getDate() {
        return date;
    }

    public String getHash() {
        return hash;
    }

    public int getVersion() {
        return version;
    }

    public long getPageVersionId() {
        return pageVersionId;
    }

    public static String getHash(ContentEntityObject page) {
        String body = page.getBodyAsString() + page.getTitle();
        return DigestUtils.sha256Hex(body);
    }

    public enum Status {
        UNSEEN, SEEN, SIGNED, OUTDATED, INVALID, IGNORED
    }

}
