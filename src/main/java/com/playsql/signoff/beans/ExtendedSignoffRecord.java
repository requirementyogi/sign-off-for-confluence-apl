package com.playsql.signoff.beans;

/*
 * #%L
 * Play SQL Sign-Off
 * %%
 * Copyright (C) 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This JSON-able object contains the information that is stored in the database ({@link SignoffRecord})
 * and adds more info, in order to send it to the front-end for display.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ExtendedSignoffRecord extends SignoffRecord {
    private final String userDisplayName;
    private final String userName;
    private Status status;
    /** "Requested By" is only present if it comes from a sign off request */
    private String requestedByUserKey;
    private String requestedByUserDisplayName;
    private String requestedByUserName;

    public ExtendedSignoffRecord(SignoffRecord record, String userName, String userDisplayName, Status status) {
        super(record);
        this.userDisplayName = userDisplayName;
        this.userName = userName;
        this.status = status;
    }

    public ExtendedSignoffRecord() {
        this.status = Status.UNSEEN;
        userName = null;
        userDisplayName = null;
    }

    public String getUserDisplayName() {
        return userDisplayName;
    }

    public String getUserName() {
        return userName;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }


    public void setRequestedBy(String userKey, String displayName, String userName) {
        this.requestedByUserKey = userKey;
        this.requestedByUserDisplayName = displayName;
        this.requestedByUserName = userName;
    }

    public String getRequestedByUserKey() {
        return requestedByUserKey;
    }

    public String getRequestedByUserDisplayName() {
        return requestedByUserDisplayName;
    }

    public String getRequestedByUserName() {
        return requestedByUserName;
    }
}
