package com.playsql.signoff.web;

/*
 * #%L
 * Play SQL Sign-Off
 * %%
 * Copyright (C) 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.actions.AbstractPageAwareAction;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.gzipfilter.org.apache.commons.lang.StringUtils;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.ContextProvider;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.playsql.signoff.beans.ExtendedSignoffRecord;
import com.playsql.signoff.beans.SignoffRecord;
import com.playsql.signoff.manager.SignoffService;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

import static com.google.common.base.Objects.equal;
import static com.playsql.signoff.Utils.*;

/**
 * A ContextProvider fetches/compiles information before it is provided to a .vm template - in the current
 * situation, it's metadata-head.vm.
 */
public class MetadataContextProvider implements ContextProvider {

    private final SignoffService service;

    public MetadataContextProvider(SignoffService service) {
        this.service = service;
    }

    @Override
    public void init(Map<String, String> map) throws PluginParseException {

    }

    @Override
    public Map<String, Object> getContextMap(Map<String, Object> map) {
        Map<String, String> metadata = Maps.newHashMap();

        Object action = map.get("action");
        if (action instanceof AbstractPageAwareAction) {
            ConfluenceUser viewer = AuthenticatedUserThreadLocal.get();
            AbstractPage page;
            page = ((AbstractPageAwareAction) action).getPage();
            if (page != null) {
                // Fetch the signatures
                TreeMap<Integer, List<ExtendedSignoffRecord>> signatures = service.getSignatures(page, false);
                // Fetch the requests - and flag the current user as SEEN
                List<ExtendedSignoffRecord> requests = service.getSignRequests(page, signatures, viewer);

                // List the signators of the current version
                List<String> currentSignators = dedupe(transform(signatures.get(-1),
                    (record) -> firstNonBlank(record.getUserDisplayName(), record.getUserName())));
                metadata.put("signoff-addon-signators", StringUtils.join(currentSignators, ", "));

                // Count how many versions were signed
                int size = signatures.size();
                metadata.put("signoff-addon-count-versions-signed", String.valueOf(size));
                if (size > 0) {
                    ExtendedSignoffRecord value = firstOf(signatures.firstEntry().getValue());
                    metadata.put("signoff-addon-older-signators", value != null ? value.getUserDisplayName() : null);
                }

                String hash = SignoffRecord.getHash(page);
                metadata.put("signoff-addon-page-hash", hash);

                metadata.put("signoff-addon-viewing-history-version", page.isLatestVersion() ? "" : String.valueOf(page.getVersion()));

                if (requests != null && !requests.isEmpty()) {
                    SignoffRecord.Status status = getCurrentUserStatus(requests, viewer);
                    metadata.put("signoff-addon-request-status", Objects.toString(status));

                    metadata.put("signoff-addon-requestor", requests.get(0).getRequestedByUserDisplayName());

                    Integer percentageCompleted = getPercentageCompleted(requests);
                    metadata.put("signoff-addon-request-percentage", Objects.toString(percentageCompleted));
                }
            }
        }

        map.put("signoffAddonMetadata", metadata);
        return map;
    }

    private Integer getPercentageCompleted(List<ExtendedSignoffRecord> requests) {
        if (requests == null || requests.isEmpty()) return null;
        int count = requests.size();
        int validated = Iterables.size(Iterables.filter(requests, (request) -> request.getStatus() == SignoffRecord.Status.SIGNED));
        if (validated == count) return 100; // Important, so it reads "Signed off"
        int percentage = (validated * 100) / count;
        return percentage;
    }

    private SignoffRecord.Status getCurrentUserStatus(List<ExtendedSignoffRecord> requests, ConfluenceUser viewer) {
        if (requests != null && viewer != null)
            for (ExtendedSignoffRecord request : requests)
                if (equal(request.getUserKey(), viewer.getKey().getStringValue()))
                    return request.getStatus();
        return null;
    }

    private List<String> dedupe(List<String> transform) {
        List<String> uniques = Lists.newArrayList();
        for(String element : transform)
            if (!uniques.contains(element))
                uniques.add(element);
        return uniques;
    }

    private <T, U> List<U> transform(List<T> list, Function<T, U> function) {
        if (list == null) return Lists.newArrayList();
        return Lists.transform(list, function);
    }
}
