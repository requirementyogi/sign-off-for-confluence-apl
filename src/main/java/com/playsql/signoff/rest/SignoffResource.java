package com.playsql.signoff.rest;

/*
 * #%L
 * Play SQL Sign-Off
 * %%
 * Copyright (C) 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.api.service.exceptions.BadRequestException;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.extras.common.log.Logger;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.playsql.signoff.beans.ExtendedSignoffRecord;
import com.playsql.signoff.beans.SignoffRecord;
import com.playsql.signoff.Utils;
import com.playsql.signoff.manager.SignoffService;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.lang.reflect.Type;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static com.google.common.base.Objects.equal;

@Path("/signoff")
public class SignoffResource extends AbstractRestResource {

    private static final Logger.Log LOG = Logger.getInstance(SignoffResource.class);
    private static final String PREFIX = "com.playsql.signoffs.page-";
    private final Gson GSON = Utils.gson();
    private final Type TYPE_TOKEN = new TypeToken<List<SignoffRecord>>() {}.getType();

    private final PageManager pageManager;
    private final BandanaManager bandanaManager;
    private final SignoffService signoffService;

    public SignoffResource(PageManager pageManager,
                           BandanaManager bandanaManager,
                           TransactionTemplate transactionTemplate,
                           SignoffService signoffService) {
        super(transactionTemplate);
        this.pageManager = pageManager;
        this.bandanaManager = bandanaManager;
        this.signoffService = signoffService;
    }

    /**
     * Add the authenticated user's signature to the page with the provided contentId.
     *
     * @param contentId the page's id (or blog post, or any other CEO)
     * @return "Signed" in plain text if successful.
     */
    @Path("/{contentId}")
    @PUT
    public Object sign(@PathParam("contentId") long contentId) {
        ConfluenceUser user = AuthenticatedUserThreadLocal.get();
        if (user == null) {
            throw new WebApplicationException(Response.status(Response.Status.FORBIDDEN).entity("User must be authenticated.").build());
        }

        return doInTransaction(() -> {
            signoffService.sign(contentId, user);
            return Response.ok("Signed").build();
        });
    }

    /**
     * Lists the signatures for the provided contentId.
     *
     * @param contentId the ID of the page, blog, etc.
     * @param checkSignatures whether or not signatures should be checked. Checking signature takes a bit more time
     *                        because they require hashing the body of the page and comparing the hash with the
     *                        signature. If false, then signatures of the current version will be checked anyway. If
     *                        true, signatures which don't match the current version will be checked against previous
     *                        versions. Default true.
     * @param currentHash the hash of the current page displayed to the user. We can't just retrieve it on the
     *                    server side because someone might have modified the page.
     * @return some JSON in the format of {@link SignoffResourceResult}
     */
    @GET
    @Path("/{contentId}")
    @Produces("application/json")
    public Object list(@PathParam("contentId") long contentId,
                       @QueryParam("checkSignatures") Boolean checkSignatures,
                       @QueryParam("currentHash") String currentHash) {
        ConfluenceUser user = AuthenticatedUserThreadLocal.get();
        if (user == null) {
            throw new WebApplicationException(Response.status(Response.Status.FORBIDDEN).entity("User must be authenticated.").build());
        }
        if (checkSignatures == null) checkSignatures = true;

        AbstractPage page = pageManager.getAbstractPage(contentId);
        if (page == null) {
            throw new BadRequestException("Can't find content #" + contentId);
        }
        if (currentHash != null && !equal(currentHash, SignoffRecord.getHash(page))) {
            throw new WebApplicationException(Response.status(Response.Status.CONFLICT).entity("Looks like some else has changed the page. Please refresh the page.").build());
        }
        TreeMap<Integer, List<ExtendedSignoffRecord>> signatures = signoffService.getSignatures(page, checkSignatures);
        // Check the hash of the current page: If the REST request was sent after the page was concurrently modified, then the "current" signatures are
        // outdated.
        if (currentHash != null) {
            List<ExtendedSignoffRecord> currentSignatures = signatures.get(-1);
            if (currentSignatures != null) {
                Iterator<ExtendedSignoffRecord> it = currentSignatures.iterator();
                while (it.hasNext()) {
                    ExtendedSignoffRecord signature = it.next();
                    if (!equal(currentHash, signature.getHash())) {
                        it.remove();
                        putInCorrectPlace(signatures, signature);
                    }
                }
            }
        }
        List<ExtendedSignoffRecord> requests = signoffService.getSignRequests(page, signatures, null);
        TreeMap<Integer, List<ExtendedSignoffRecord>> signaturesOutsideRequests = filterSignaturesOutsideRequests(signatures, requests);

        return Response.ok(new SignoffResourceResult(signatures, requests, signaturesOutsideRequests)).build();
    }

    private TreeMap<Integer, List<ExtendedSignoffRecord>> filterSignaturesOutsideRequests(
        TreeMap<Integer, List<ExtendedSignoffRecord>> signatures,
        List<ExtendedSignoffRecord> requests) {

        if (requests == null) return signatures;
        TreeMap<Integer, List<ExtendedSignoffRecord>> result = new TreeMap<>();
        for (Map.Entry<Integer, List<ExtendedSignoffRecord>> entry : signatures.entrySet()) {
            List<ExtendedSignoffRecord> list = entry.getValue();
            List<ExtendedSignoffRecord> resultList = Lists.newArrayList();
            if (list != null)
                // Pour all elements from list into resultList
                for (ExtendedSignoffRecord record : list)
                    // Check whether the user is already in 'requests'
                    if (Iterables.find(requests, (request) -> equal(record.getUserKey(), request.getUserKey()), null) == null)
                        resultList.add(record);
            if (!resultList.isEmpty()) {
                result.put(entry.getKey(), resultList);
            }
        }
        return result;
    }

    private void putInCorrectPlace(TreeMap<Integer, List<ExtendedSignoffRecord>> signatures, ExtendedSignoffRecord signature) {
        int key = signature.getVersion();
        List<ExtendedSignoffRecord> list = signatures.get(key);
        if (list == null) {
            signatures.put(key, Lists.newArrayList(signature));
        } else {
            list.add(signature);
        }
    }
}
