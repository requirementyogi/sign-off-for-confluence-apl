package com.playsql.signoff.rest;

/*
 * #%L
 * Play SQL Sign-Off
 * %%
 * Copyright (C) 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.playsql.signoff.beans.ExtendedSignoffRecord;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.TreeMap;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class SignoffResourceResult {
    private final TreeMap<Integer, List<ExtendedSignoffRecord>> signatures;
    private final List<ExtendedSignoffRecord> requests;
    private final TreeMap<Integer, List<ExtendedSignoffRecord>> signaturesOutsideRequests;

    public SignoffResourceResult(TreeMap<Integer,
        List<ExtendedSignoffRecord>> signatures,
                                 List<ExtendedSignoffRecord> requested,
                                 TreeMap<Integer, List<ExtendedSignoffRecord>> signaturesOutsideRequests) {
        this.signatures = signatures;
        this.requests = requested;
        this.signaturesOutsideRequests = signaturesOutsideRequests;
    }

    public TreeMap<Integer, List<ExtendedSignoffRecord>> getSignatures() {
        return signatures;
    }

    public List<ExtendedSignoffRecord> getRequests() {
        return requests;
    }

    public TreeMap<Integer, List<ExtendedSignoffRecord>> getSignaturesOutsideRequests() {
        return signaturesOutsideRequests;
    }
}
