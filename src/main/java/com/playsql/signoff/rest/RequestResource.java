package com.playsql.signoff.rest;

/*
 * #%L
 * Play SQL Sign-Off
 * %%
 * Copyright (C) 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.atlassian.confluence.api.service.exceptions.BadRequestException;
import com.atlassian.confluence.mail.template.ConfluenceMailQueueItem;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.core.task.MultiQueueTaskManager;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.user.Group;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.playsql.signoff.beans.SignoffRequest;
import com.playsql.signoff.Utils;
import com.playsql.signoff.manager.SignoffService;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

/**
 * Manages requests for signatures. Like, email and notifications.
 */
@Path("/request")
public class RequestResource extends AbstractRestResource {
    private final PageManager pageManager;
    private final ApplicationProperties applicationProperties;
    private final MultiQueueTaskManager taskManager;
    private final UserAccessor userAccessor;
    private final SignoffService service;

    public RequestResource(TransactionTemplate transactionTemplate,
                           PageManager pageManager,
                           ApplicationProperties applicationProperties,
                           MultiQueueTaskManager taskManager,
                           UserAccessor userAccessor,
                           SignoffService service) {
        super(transactionTemplate);
        this.pageManager = pageManager;
        this.applicationProperties = applicationProperties;
        this.taskManager = taskManager;
        this.userAccessor = userAccessor;
        this.service = service;
    }

    public static class Payload {
        public String message;
        public List<String> recipients;
        public boolean sendMail;

        public String getMessage() {
            return message;
        }

        public List<String> getRecipients() {
            return recipients;
        }

        public boolean isSendMail() {
            return sendMail;
        }
    }

    /** This method allows a user to reject a signature request */
    @DELETE
    @Path("/{contentId}")
    public Response ignoreRequest(@PathParam("contentId") long contentId) {
        ConfluenceUser user = AuthenticatedUserThreadLocal.get();
        if (user == null) {
            throw new WebApplicationException(Response.status(Response.Status.FORBIDDEN).entity("User must be authenticated.").build());
        }

        AbstractPage page = pageManager.getAbstractPage(contentId);
        if (page == null) {
            throw new BadRequestException("Can't find content #" + contentId);
        }

        service.ignoreRequest(page, user);
        return Response.ok("Ok, sign off request is ignored for " + user.getName()).build();

    }

    /**
     * Send a request from the current user to recipients with the provided message.
     */
    @PUT
    @Path("/{contentId}")
    public Response sendRequest(@PathParam("contentId") long contentId,
                                Payload payload) {

        ConfluenceUser author = AuthenticatedUserThreadLocal.get();
        if (author == null) {
            throw new WebApplicationException(Response.status(Response.Status.FORBIDDEN).entity("User must be authenticated.").build());
        }
        if (StringUtils.isBlank(author.getEmail())) {
            throw new WebApplicationException(Response.status(Response.Status.FORBIDDEN).entity("Please set your email before sending to other people.").build());
        }

        AbstractPage page = pageManager.getAbstractPage(contentId);
        if (page == null) {
            throw new BadRequestException("Can't find content #" + contentId);
        }

        StringBuilder url = new StringBuilder(Utils.stripAnchor(page.getUrlPath()));

        String baseUrl = applicationProperties.getBaseUrl(UrlMode.ABSOLUTE);
        url.insert(0, baseUrl);
        url.append("#signoff-request");

        if (payload == null || payload.getRecipients() == null || payload.getRecipients().isEmpty()) {
            throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST).entity("No recipient was provided.").build());
        }
        List<ConfluenceUser> recipients = Lists.newArrayList();
        List<String> payloadRecipientList = payload.getRecipients();
        for (String recipient : payloadRecipientList) {
            if (recipient.startsWith("user-")) {
                String userKey = recipient.substring("user-".length());
                ConfluenceUser recipientUser = userAccessor.getUserByKey(new UserKey(userKey));
                if (recipientUser == null) {
                    throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST).entity("User " + userKey + " does not exist.").build());
                }
                if (StringUtils.isNotBlank(recipientUser.getEmail())) {
                    if (!recipients.contains(recipientUser))
                        recipients.add(recipientUser);
                }
            } else if (recipient.startsWith("group-")) {
                String groupName = recipient.substring("group-".length());
                Group group = userAccessor.getGroup(groupName);
                if (group == null) {
                    throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST).entity("Group " + groupName + " does not exist.").build());
                }
                Iterable<ConfluenceUser> members = userAccessor.getMembers(group);
                if (members != null) for (ConfluenceUser member : members) {
                    if (StringUtils.isNotBlank(member.getEmail())) {
                        if (!recipients.contains(member))
                            recipients.add(member);
                    }
                }
            } else {
                throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST).entity("Recipient " + recipient + " is neither a user nor a group.").build());
            }
        }

        if (payload.isSendMail()) {
            for (ConfluenceUser recipient : recipients) {
                sendMail(page, url.toString(), payload.getMessage(), author, recipient);
            }
        }

        // Now that we've validated payloadRecipientList, we can safely save it.
        SignoffRequest request = new SignoffRequest(author.getKey().getStringValue(), payloadRecipientList);
        service.saveRequest(page, request);

        return Response.ok().entity(Lists.transform(recipients, (user) -> "\"" + user.getFullName() + "\" <" + user.getEmail() + ">")).build();
    }

    private void sendMail(AbstractPage page, String url, String message, ConfluenceUser author, ConfluenceUser recipient) {
        String messageHtml = GeneralUtil.plain2html(message);
        Map<String, Object> context = Maps.newHashMap();
        context.put("pageUrl", url.toString());
        context.put("page", page);
        context.put("author", author);
        context.put("messageHtml", messageHtml);
        context.put("generalUtil", new GeneralUtil());
        String html = VelocityUtils.getRenderedTemplate("signoff-email-template.vm", context);

        ConfluenceMailQueueItem mail = new ConfluenceMailQueueItem(
                recipient.getEmail(),
                "Sign-off Request: " + page.getDisplayTitle(),
                html,
                ConfluenceMailQueueItem.MIME_TYPE_HTML);
        taskManager.addTask("mail", mail);
    }
}
