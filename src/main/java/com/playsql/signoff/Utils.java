package com.playsql.signoff;

/*
 * #%L
 * Play SQL Sign-Off
 * %%
 * Copyright (C) 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.gson.Gson;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.util.Iterator;

public class Utils {

    private final static Logger log = Logger.getLogger(Utils.class);
    public static final Gson GSON = new Gson();

    public static <T> T firstOf(Iterable<T> t) {
        if (t != null) {
            Iterator<T> it = t.iterator();
            if (it.hasNext()) {
                return it.next();
            }
        }
        return null;
    }

    public static String firstNonBlank(Object... values) {
        if (values != null)
            for (Object value : values)
                if (value instanceof String && StringUtils.isNotBlank((String) value))
                    return (String) value;
        return null;
    }

    public static String stripAnchor(String urlPath) {
        if (urlPath == null) return null;
        int index = urlPath.indexOf("#");
        if (index != -1) {
            return urlPath.substring(0, index);
        }
        return urlPath;
    }

    public static Gson gson() {
        return GSON;
    }
}






