package com.playsql.signoff.manager;

/*
 * #%L
 * Play SQL Sign-Off
 * %%
 * Copyright (C) 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.api.service.exceptions.BadRequestException;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.DefaultSaveContext;
import com.atlassian.confluence.core.Modification;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.plugins.createcontent.SpaceBandanaContext;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.extras.common.log.Logger;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.user.Group;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.playsql.signoff.beans.SignoffRecord;
import com.playsql.signoff.beans.ExtendedSignoffRecord;
import com.playsql.signoff.beans.SignoffRequest;
import com.playsql.signoff.Utils;
import com.playsql.signoff.rest.SignoffResource;
import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nullable;
import java.lang.reflect.Type;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;

import static com.google.common.base.Objects.equal;
import static com.playsql.signoff.beans.SignoffRecord.Status.*;

public class SignoffService {
    private static final Logger.Log LOG = Logger.getInstance(SignoffResource.class);
    private static final String PREFIX_SIGNOFF = "com.playsql.signoff.signoffs.page-";
    private static final String PREFIX_REQUESTS = "com.playsql.signoff.requests.page-";
    private final Gson GSON = Utils.gson();
    private final Type TYPE_TOKEN_RECORD = new TypeToken<List<SignoffRecord>>() {}.getType();
    private final Type TYPE_TOKEN_REQUEST = new TypeToken<List<SignoffRequest>>() {}.getType();

    private final PageManager pageManager;
    private final BandanaManager bandanaManager;
    private final UserAccessor userAccessor;


    public SignoffService(PageManager pageManager,
                          BandanaManager bandanaManager,
                          UserAccessor userAccessor) {
        this.pageManager = pageManager;
        this.bandanaManager = bandanaManager;
        this.userAccessor = userAccessor;
    }

    public void sign(long contentId, ConfluenceUser user) {
        String humanName = Utils.firstNonBlank(user.getFullName(), user.getName(), user.getKey());
        AbstractPage page = pageManager.getAbstractPage(contentId);
        if (page == null) {
            throw new BadRequestException("Can't find content #" + contentId);
        }
        // Create a new version of the page (only if the previous version wasn't already a mirror of the current one)
        ContentEntityObject previousVersion = pageManager.getPreviousVersion(page);
        if (previousVersion == null || !equal(SignoffRecord.getHash(previousVersion), SignoffRecord.getHash(page))) {
            pageManager.saveNewVersion(page, new NoopModification(), new DefaultSaveContext(false, true, false));
            previousVersion = pageManager.getPreviousVersion(page);
            if (previousVersion == null) {
                throw new NotImplementedException("Could not create a new version for: " + page.getId());
            }
        }
        if (StringUtils.isNotBlank(previousVersion.getVersionComment())) {
            previousVersion.setVersionComment(previousVersion.getVersionComment()
                + " / Signed off by " + humanName);
        } else {
            previousVersion.setVersionComment("Signed off by " + humanName);
        }
        //ceo.setVersionComment("No change");
        SignoffRecord record = new SignoffRecord(user, GregorianCalendar.getInstance().getTime(),
            previousVersion.getVersion(), previousVersion.getId(),
            SignoffRecord.getHash(previousVersion));

        bandana(page, records -> {
            records.add(record);
            return true;
        });

        // When a user signs, they're removed from any previous sign request (whether they're in viewed or refusal)
        bandanaForRequests(page, requests -> {
            boolean changed = false;
            String userKey = "user-" + user.getKey().getStringValue();
            for (SignoffRequest request : requests) {
                if (request.getViewed() != null && request.getViewed().remove(userKey)) changed = true;
                if (request.getRefusals() != null && request.getRefusals().remove(userKey)) changed = true;
            }
            return changed;
        });
    }

    /**
     * Returns all signatures for this page.
     *
     * If the page is a history version, then signatures of that version will be returned with key -1
     *
     * @param page the page
     * @return the signatures, indexed by page version. All signatures for the current version are returned with
     * the key {@code -1}.
     * @throws BadRequestException if the page can't be found.
     */
    public TreeMap<Integer, List<ExtendedSignoffRecord>> getSignatures(AbstractPage page, boolean checkForAllVersions) {
        AtomicReference<TreeMap<Integer, List<ExtendedSignoffRecord>>> result = new AtomicReference<>();
        AbstractPage latestPage = page.getLatestVersion();
        bandana(latestPage, records -> {
            if (!records.isEmpty()) {
                final String currentHash = SignoffRecord.getHash(page);
                TreeMap<Integer, List<ExtendedSignoffRecord>> map = new TreeMap<>();
                records.forEach((record) -> {
                    ConfluenceUser user = userAccessor.getUserByKey(new UserKey(record.getUserKey()));
                    boolean validHash = true;
                    Integer key = record.getVersion();
                    SignoffRecord.Status status = null;
                    if (equal(record.getHash(), currentHash)) {
                        key = -1;
                        status = SIGNED;
                    } else if (checkForAllVersions) {
                        // We are asked to retrieve the old version of the page and check the hash is correct
                        AbstractPage previousVersion = pageManager.getAbstractPage(record.getPageVersionId());
                        if (previousVersion == null || !equal(SignoffRecord.getHash(previousVersion), record.getHash())) {
                            // Hashes don't match, don't include this hash.
                            validHash = false;
                            status = INVALID;
                        } else {
                            status = OUTDATED;
                        }
                    } else {
                        status = OUTDATED; // The real status here is "unchecked"
                    }
                    ExtendedSignoffRecord extendedRecord = new ExtendedSignoffRecord(record, user.getName(), user.getFullName(), status);
                    List<ExtendedSignoffRecord> currentList = map.get(key);
                    if (currentList == null) {
                        map.put(key, Lists.newArrayList(extendedRecord));
                    } else {
                        currentList.add(extendedRecord);
                    }
                });
                result.set(map);
            } else {
                result.set(new TreeMap<>());
            }
            return false; // Don't save
        });
        return result.get();
    }

    /**
     * Returns the list of users who were requested to sign off, along with a status, and who requested their sign off
     * @param page
     * @param signatures
     * @param markAsViewed if non null, then this user will be marked as having viewed the page if they haven't signed off yet.
     * @return
     */
    @Nullable
    public List<ExtendedSignoffRecord> getSignRequests(AbstractPage page, TreeMap<Integer, List<ExtendedSignoffRecord>> signatures, ConfluenceUser markAsViewed) {
        AtomicReference<List<ExtendedSignoffRecord>> result = new AtomicReference<>();
        AbstractPage latestPage = page.getLatestVersion();
        bandanaForRequests(latestPage, requests -> {
            boolean changed = false;
            if (!requests.isEmpty()) {
                String hash = SignoffRecord.getHash(page);
                List<ExtendedSignoffRecord> recipients = Lists.newArrayList();
                List<SignoffRequest> requestsByNewestFirst = Lists.reverse(requests);
                for (SignoffRequest request : requestsByNewestFirst) {

                    Supplier<List<ConfluenceUser>> viewed = Suppliers.memoize(() -> decodeRecipients(request.getViewed()));
                    Supplier<List<ConfluenceUser>> refusals = Suppliers.memoize(() -> decodeRecipients(request.getRefusals()));
                    List<ConfluenceUser> users = decodeRecipients(request.getRecipients());

                    for (ConfluenceUser user : users) {
                        String userKey = user.getKey().getStringValue();
                        // If the user was already seen in another signoff request, then we'll get the same results if we process it again
                        if (Iterables.find(recipients, (record) -> equal(userKey, record.getUserKey()), null) != null) {
                            continue;
                        }
                        SignoffRecord.Status status;
                        if (refusals.get().contains(user)) {
                            status = IGNORED;
                        } else if (viewed.get().contains(user)) {
                            status = SEEN;
                        } else {
                            status = UNSEEN;
                        }
                        ExtendedSignoffRecord latestSignature = null;
                        for (List<ExtendedSignoffRecord> list : signatures.values()) {
                            for (ExtendedSignoffRecord record : list) {
                                if (equal(record.getUserKey(), user.getKey().getStringValue())) {
                                    if (latestSignature == null
                                        || latestSignature.getVersion() < record.getVersion()
                                        || latestSignature.getVersion() == record.getVersion() && latestSignature.getStatus() != SIGNED && record.getStatus() != SIGNED) {
                                        latestSignature = record;
                                        status = record.getStatus();
                                    }
                                }
                            }
                        }
                        if (latestSignature == null) {
                            // The user hasn't signed yet
                            latestSignature = new ExtendedSignoffRecord(new SignoffRecord(user, null, -1, page.getId(), null), user.getName(), user.getFullName(), status);
                            if (equal(user, markAsViewed) && status == UNSEEN) {
                                // Mark as seen
                                latestSignature.setStatus(SEEN);
                                request.addViewed("user-" + user.getKey().getStringValue());
                                changed = true;
                            }
                        } else {
                            if (latestSignature.getStatus() == SIGNED && !equal(latestSignature.getHash(), hash)) {
                                status = OUTDATED;
                            }
                            latestSignature.setStatus(status);
                        }


                        ConfluenceUser author = userAccessor.getUserByKey(new UserKey(request.getAuthorKey()));
                        String authorDisplayName = author != null ? author.getFullName() : null;
                        String authorName = author != null ? author.getName() : null;
                        latestSignature.setRequestedBy(request.getAuthorKey(), authorDisplayName, authorName);
                        recipients.add(latestSignature);
                    }
                }
                result.set(recipients);
            }
            return changed; // The user can be marked as "SEEN"
        });

        List<ExtendedSignoffRecord> recipients = result.get();
        if (recipients != null)
            Collections.sort(recipients, (o1, o2) ->
                100 * (ifNegativeThenMaxInt(o2.getVersion()) - ifNegativeThenMaxInt(o1.getVersion()))
                    + ordering(o2.getStatus()) - ordering(o1.getStatus()));
        return result.get();
    }

    private int ifNegativeThenMaxInt(int version) {
        if (version < 0)
            return Integer.MAX_VALUE;
        return version;
    }

    private static int ordering(SignoffRecord.Status s) {
        switch (s) {
            case SIGNED: return 1;
            case SEEN: return 2;
            case OUTDATED: return 3;
            case INVALID: return 5;
            case IGNORED: return 6;
            case UNSEEN: return 8;
            default: return 10;
        }
    }

    private List<ConfluenceUser> decodeRecipients(List<String> recipients) {
        List<ConfluenceUser> users = Lists.newArrayList();
        if (recipients != null) {
            for (String recipient : recipients) {
                if (recipient.startsWith("user-")) {
                    String userKey = recipient.substring("user-".length());
                    ConfluenceUser user = userAccessor.getUserByKey(new UserKey(userKey));
                    if (user != null && !users.contains(user)) {
                        users.add(user);
                    }
                } else if (recipient.startsWith("group-")) {
                    String groupName = recipient.substring("group-".length());
                    Group group = userAccessor.getGroup(groupName);
                    if (group != null) {
                        Iterable<ConfluenceUser> members = userAccessor.getMembers(group);
                        if (members != null) for (ConfluenceUser member : members) {
                            if (StringUtils.isNotBlank(member.getEmail())) {
                                if (member != null && !users.contains(member)) {
                                    users.add(member);
                                }
                            }
                        }
                    }
                } else {
                    LOG.warn("When decoding sign-off requests, a userdef doesn't make sense: " + recipient);
                }
            }
        }
        return users;
    }

    public void saveRequest(AbstractPage page, SignoffRequest request) {
        bandanaForRequests(page, (records) -> {
            // Must be from oldest to newest
            records.add(request);
            return true;
        });
    }

    public void ignoreRequest(AbstractPage page, ConfluenceUser user) {
        if (user == null) return;
        AtomicReference<List<ExtendedSignoffRecord>> result = new AtomicReference<>();
        AbstractPage latestPage = page.getLatestVersion();
        bandanaForRequests(latestPage, requests -> {
            if (requests == null || requests.isEmpty()) return false;
            boolean changed = false;
            String key = "user-" + user.getKey().getStringValue();
            List<SignoffRequest> requestByLatestFirst = Lists.reverse(requests);
            for (SignoffRequest request : requestByLatestFirst) {
                changed = request.addRefusal(key) || changed;
            }
            return changed;
        });
    }

    private static class NoopModification implements Modification<ContentEntityObject> {
        @Override
        public void modify(ContentEntityObject content) {
            // There is no modification in the new page.
        }
    }

    /**
     * Run the callback with the SignoffRecords of the page. If modified, saves the records.
     * @param callback return true if the records must be saved.
     */
    private void bandana(AbstractPage page, Function<List<SignoffRecord>, Boolean> callback) {
        bandanaGeneric(page, SignoffRecord.class, TYPE_TOKEN_RECORD, callback);
    }

    /**
     * Run the callback with the SignoffRequests of the page. If modified, saves the records.
     * @param callback return true if the records must be saved.
     */
    private void bandanaForRequests(AbstractPage page, Function<List<SignoffRequest>, Boolean> callback) {
        bandanaGeneric(page, SignoffRequest.class, TYPE_TOKEN_REQUEST, callback);
    }

    private <T> void bandanaGeneric(AbstractPage page, Class<T> clazz, Type token, Function<List<T>, Boolean> callback) {
        if (!page.isLatestVersion()) {
            throw new IllegalArgumentException("The sign-off service requires the latest page.");
        }
        SpaceBandanaContext context = new SpaceBandanaContext(page.getSpace());
        String prefix = clazz.equals(SignoffRecord.class) ? PREFIX_SIGNOFF
            : clazz.equals(SignoffRequest.class) ? PREFIX_REQUESTS
            : null;
        String key = prefix + page.getIdAsString();
        Object string = bandanaManager.getValue(context, key);
        List<T> list = null;
        if (string instanceof String && StringUtils.isNotBlank((String) string)) {
            try {
                list = GSON.fromJson((String) string, token);
            } catch (JsonSyntaxException jse) {
                list = null;
            }
        }
        if (list == null) {
            list = Lists.newArrayList();
        }
        Boolean result = callback.apply(list);
        if (result) {
            if (list.size() == 0) {
                bandanaManager.removeValue(context, key);
            } else {
                String json = GSON.toJson(list, token);
                bandanaManager.setValue(context, key, json);
            }
        }
    }
}
