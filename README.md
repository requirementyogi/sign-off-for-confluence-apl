# README #

This is the code for Play SQL's Sign-Off add-on for Atlassian Confluence Server.

We've discontinued the product on the Atlassian Marketplace because we haven't had enough interest and we don't want to provide professional support given this low revenue. However, we're happy to publish it as open-source for free, so evaluators can keep it and maintain it themselves, and it can help anyone who wants to build an add-on.

### What is this repository for? ###

As described on: https://marketplace.atlassian.com/plugins/com.playsql.signoff/server/overview

This is a very straightforward approval add-on: It adds a "Sign Off" button at the top of the page, so your stakeholders can sign off on your documents.

There are 2 modes:

* Sign-off: It displays a list of signatures. If someone updates the page again, old signatures appear as outdated, and they link to the history version of the page.
* Requests: Send a request to groups or individuals, and you can track individually who has seen and who has signed the page. Also, a prominent button says "Signature Requested" for users who are expected to sign.

Use-cases:

This can help improving the commitment of your client by enforcing an approval workflow,
This can be used by a CEO to check the all employees have seen a new policy.
Are you using sign-off for specifications? Check out [Requirement Yogi](https://marketplace.atlassian.com/plugins/com.playsql.requirementyogi/server/overview).

### How do I get set up? ###


It works well with Java 8 and Maven 3.0.5.

```
#!bash

git clone git@bitbucket.org:aragot/sign-off-for-confluence-apl.git
cd sign-off-for-confluence-apl
mvn clean install # to check the compilation works.
mvn amps:debug # to start Confluence
mvn amps:cli # to reinstall the add-on each time you make changes during development.
```

### How to build a release ###

It depends where you want to put the final artifacts. We like to put them in a ```releases``` directory adjacent to the git repo, so here's what we do:

```
#!bash
cd sign-off-for-confluence-apl
# This will put 1.3.2 in the ../releases directory,
# commit the version in the git repository, and
# reset the pom.xml to 1.3-SNAPSHOT after the release.
./release.sh 1.3.2 1.3-SNAPSHOT
```

### What you can and can't do ###

Here is an explanation of the license. The license is Apache Public License v2.0 and is one of the most used open-source license because it's compatible with most business models. You can use and modify the product. If you make changes, you don't have to contribute back to this repo, but it would be a nice thing to do for the community.

#### Can I change the product and sell it on the Marketplace? ####

Yes ! It's open-source, so you can sell it. However, you must provide the source to anyone who has received the compiled version. Given there's a 1-month evaluation of the product on the Atlassian Marketplace during which customers receive the compiled version, all evaluators are supposed to get the source too. You can try selling it as APL on the Marketplace, assuming people won't bypass the payment requirement because they honestly want you to keep working on the product.

#### Do I have to publish new code under "Play SQL" copyright? ####

No. New files you write are automatically given a "Play SQL copyright" header by the build tool, which you can override if it's your code. You keep your copyright on, for example, new classes you write, and you don't have to publish them with the source, provided you comply with the APL license.

### Contribution guidelines ###

Create a pull-request. Thank you very much! Please write features which can be reused by other companies, where parameters can be modified through configuration.

* This project is looking for a new owner. Be the one!

* It would be great if someone replaced the vendor logo and name by something else, preferably someone who accepts to endorse the maintenance.

### Copyright and endorsement ###

The first version was fully written by Play SQL S.A.S.U. It's nice if you tell that we're the original author, but it should not appear as an endorsement from us.

Atlassian and Confluence are reserved trademarks of Atlassian - See https://www.atlassian.com/legal/trademark

### Who do I talk to? ###

Adrien Ragot is the owner of this repository. However, neither Adrien Ragot nor Play SQL S.A.S.U. promise to maintain or provide support for this product.